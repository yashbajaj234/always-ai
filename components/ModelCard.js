import { Grid } from '@material-ui/core';
import { ModelCardStyles } from './styles/ModelStyles'
// import Image from 'next/image'

const baseModelUrl = `https://console.alwaysai.co/model-catalog?model=`;
export default function ModelCard({ model }) {

  function getLabels({ labels }) {
    if (labels == undefined)
      labels = []
    let html = labels.length < 5 ? labels.map((item) => item) : labels.slice(0,5).map((item) => item);
    return html.join(',');
  }

  return (
    <ModelCardStyles >
      <Grid container spacing={2}>
        <Grid item xs={12} md={3} >
          <div className='img-wrapper'>
            <img src={model.bgImage} alt={model.id} />
            <p className={`${model.model_parameters.purpose}`}>{model.model_parameters.purpose}</p>
          </div>

        </Grid>
        <Grid container item xs={12} md={9} >
          <Grid item xs={12} md={8}>
            <a href={`${baseModelUrl}/${model.id}`} className='model-title'>{model.id}</a>
            <p className='description'>{model.description}</p>
            <div><p className='labels font-weight-bold'>Labels: </p> {!model.labels ? <span className='font-weight-normal'>Unavailable</span> : <span className='font-weight-normal'>{getLabels(model.labels)}</span>}
            </div>
          </Grid>
          <Grid item xs={12} md={4}>
            <p className='subtitle'>AVG. INFERENCE TIME </p>
            <div className='outer-ring'>
              <div className='inner-ring'>
                <p className='time-value'>{Math.round(model.inference_time * 100)/100}</p>
                 <span className='subtitle'>ms</span> 
              </div>
            </div>
            <div className='sub-stats'>
              <p className='subtitle'>V{model.version}</p>
              <p className='subtitle'>{Math.floor(model.size / (1024 * 1024))} MB</p>
            </div>
          </Grid>
         
          <hr/>
          <div className='model-footer'>
            <p>Framework : {model.model_parameters.framework_type}</p>
            <p>Data Set : {model.dataset || 'Unknown'} </p>
            <a className='use-this-model' href={`${baseModelUrl}/${model.id}`}>+ Use this Model</a>
          </div>
        </Grid>
      </Grid>
    </ModelCardStyles>
  )
}
