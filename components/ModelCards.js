import ModelCard from "./ModelCard";
import { ModelWrapperStyles } from "./styles/ModelStyles";

export default function ModelCards({models}) {
    return (
        <ModelWrapperStyles>
            {models.map(model => <ModelCard key={model.id} model={model} />)}
        </ModelWrapperStyles>
    )
}
