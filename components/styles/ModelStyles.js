import styled from "styled-components";

export const ModelWrapperStyles = styled.div`
    padding:0;
`;

export const ModelCardStyles = styled.div`
    display: flex;
    color: var(--blue);
    width: 100%;
    max-width: 910px;
    border-radius: 5px;
    box-shadow: 0px 2px 4px 0px rgb(0 0 0 / 12%);
    padding: 15px;
    margin: auto;
    transition: all .1s ease-in-out;
    margin-bottom: 1.5rem;
    font-size: 1rem;

    .description {
        max-height: 55px;
        overflow: hidden;
    }

    &:hover {
        box-shadow: 0 2px 8px 3px rgb(0 0 0 / 12%)
    }

    @media(max-width:960px) {
        img {
            max-height: 160px;
        }
    }
    img {
        height:100%;
        width: 100%;
        object-fit: cover;
    }

    .img-wrapper {
        position: relative;
        >p{
            color: var(--white);
            text-align: center;
            position: absolute;
            bottom: 0;
            left: 0;
            margin: 0;
            width: 100%;
            text-transform: uppercase;
            padding:7px;
            font-size: 12px;
            font-weight:bold;
        }
        .ObjectDetection{
            background-color: var(--ObjectDetection)
        }
        .PoseEstimation{
            background-color: var(--PoseEstimation)
        }
        .SemanticSegmentation{
            background-color: var(--SemanticSegmentation)
        }
        .Classification{
            background-color: var(--Classification)
        }

    }

    .sub-stats {
        display: flex;
        justify-content: space-around;
    }

    .time-value {
        color: var(--neon-blue);
        margin-bottom:0;
    }

    .time-value+.subtitle {
        text-transform:none;
    }

    .subtitle {
        font-size: 11px ;
        color: var(--ink-dark);
        text-transform: uppercase;
        text-align:center;
    }

    .font-weight-bold {
        font-weight: bold;
    }

    .labels {
        display: inline;
    }

    .font-weight-normal {
        font-weight: normal;
    }

    .model-title {
        font-size: 1rem;
        font-weight: bold;
        color: var(--berry-darker);
        margin:0 0 12px 0;
        cursor:pointer;
        width: 70%;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    .use-this-model {
        color:#A30640;
        font-size: 14px;
        align-self: flex-end;
    }

    .model-footer {
        display: flex;
        font-size: 11px;
        color:var(--light-grey);
        justify-content: space-around;
        width:100%;

        p {
            margin-bottom:0;
        }
      
    }

    hr {
            width: 100%;
            margin:auto;
            height: 0px;
            border:0.5px solid #E8ECED;
        }

    .inner-ring {
        position: absolute;
        left:50%;
        top:50%;
        transform:translate(-50%,-50%);
        width:64px;
        height:64px;
        border-radius:50%;
        box-shadow:0 2px 8px 0 rgb(0 0 0 / 12%);
        background:white;
        text-align:center;
    }

    .outer-ring {
        position: relative;
        width: 92px;
        height: 92px;
        border-radius: 50%;
        background: #F7F9FA;
        margin: auto;
    }
`;