import styled from "styled-components";

export const FilterContainer = styled.div`
    margin-top: 3rem;
    display: flex;
    flex-direction: column;
    
    .checked {
        color: var(--pink);
    }

    h5{
        font-size: 1rem;
    }
`;

export const FilterHeader = styled.div`

    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color:var(--lightPink);
    padding: 10px;
    border-radius:10px 10px 0 0;

    h4{
        color:var(--black);
        margin: 0;
    }

    button {
        color:var(--red);
    }
`;





