import styled from "styled-components";

export const ResultCount = styled.p`
    margin-top: 3rem;
    color: var(--grey) ;
`;