import { useFilter } from "../hooks/useFilter";
import { Checkbox, FormControlLabel } from '@material-ui/core';
import { FilterHeader,FilterContainer } from "./styles/FilterStyles";

export default function Filter({applyFilter}) {
       
    let {checked,filterSettings,setFilter,resetFilter} = useFilter(applyFilter);
    return (
        <FilterContainer>
            <FilterHeader>
                <h4>Refine By</h4>  
                <button onClick={resetFilter}>
                    Clear Filters
                </button>
            </FilterHeader>
            <h5>Framework</h5>
            {Object.entries(filterSettings).map(([key,value]) => (


                <FormControlLabel
                    key={key}
                    control={
                        <Checkbox
                        classes={{checked:'checked'}}
                            checked={checked[key]}
                            onChange={setFilter}
                            value={key}
                            id={key}
                        />
                    }
                    label={`${key} ${value}`}
                />
                // <label key={key} htmlFor={key}> 
                //     <input type='checkbox' checked={checked[key]} value={key} id={key} onChange={setFilter}/> {key} {value}
                //  </label>
                ))}

        </FilterContainer>
    )
}
