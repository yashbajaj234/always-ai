import { useState } from "react";
import { filters as initialFilters } from "../utils";
import { results as initialModels } from "../utils";
import { initialCheckedState } from '../utils';

export function useFilter( applyFilter ) {
    const [filterSettings,setFilterSettings] = useState(initialFilters);
    const [checked,setChecked] = useState(initialCheckedState);

    function resetFilter() {
        applyFilter(initialModels);
        setFilterSettings(initialFilters);
        setChecked(initialCheckedState);
    }

    function setFilter(e) {
        if(!e.target.checked)
            {   
                resetFilter();
                return;
            }
        
        let filteredModels = initialModels.filter(model => model.model_parameters.framework_type == e.target.value);
        applyFilter(filteredModels);
        setFilterSettings({[e.target.value]:initialFilters[e.target.value]})
        setChecked({
            ...initialCheckedState,
            [e.target.value]:true}
            );
    }

    return {filterSettings, resetFilter , setFilter, checked,setChecked}

}