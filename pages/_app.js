// import '../styles/globals.css'
import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle` 
  html {
    --red: #cf0a17;
    --black: #040404;
    --lightPink:#ffebeb;
    --pink:#c92c56;
    --blue:#2f3235;
    --ink-dark:#6b727a;
    --grey: #737373;
    --light-grey:#6A727B;
    --light-gray:var(--light-gray);
    --gray: var(--grey);
    --berry-darker:#740c32;
    --ObjectDetection:#65822c;
    --PoseEstimation:#2a6c82;
    --SemanticSegmentation:#82472b;
    --Classification:#822a50;
    --neon-blue:#2BD5BE;
    --white:#fff;
    --bs: 0 12px 24px 0 rgba(0,0,0,0.09);
    box-sizing: border-box;
    overflow-x: hidden;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    font-family: 'Karla';
    padding: 0;
    margin: 0;
    font-size: 16px;
  }
  a {
    text-decoration: none;
    color: var(---black);
  }
  a:hover {
    text-decoration: underline;
  }
  button {
    outline: none;
    border: none;
    background: transparent;
    cursor: pointer;
  }
`;


function MyApp({ Component, pageProps }) {
  return( 
    <>
    <GlobalStyles />
    <Component {...pageProps} />
    </>
    )
}

export default MyApp
